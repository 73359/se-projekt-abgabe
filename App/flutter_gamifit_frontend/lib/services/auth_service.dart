import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_gamifit_frontend/models/user.dart';
import 'package:flutter_gamifit_frontend/services/database.dart';

class AuthService {

  final FirebaseAuth _firebaseAuth;

  AuthService(this._firebaseAuth);

  //create user obj based on firebase obj
  GMFTUser _userFromFirebaseUser(User user) {
    return user != null ? GMFTUser(uid: user.uid, emailVerified: user.emailVerified) : null;
  }

  //auth change user stream
  Stream<User> get authStateChanges => _firebaseAuth.authStateChanges();

  //sign in with email & password
  Future login({String email, String password}) async {
    try {
      UserCredential userCredential = await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
      User user = userCredential.user;
      print ('Signed in');
      return user;
    } on FirebaseAuthException catch (e) {
      print(e);
      if (e.code == 'user-not-found') {
        print('No user found for that email.');
        return null;
      } else if (e.code == 'wrong-password') {
        print ('Wrong password provided for that user.');
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  //register with email & password
  Future register({String email, String password}) async {
    try {
      UserCredential userCredential = await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);
      User user = userCredential.user;
      // send verification mail to email
      verifyUser(user);
      // create username from email
      String username = email.substring(0, email.indexOf('@'));
      // create new document for user with uid
      await DatabaseService(uid: user.uid).updateUserData(username, 0, 0.0, 0, true, []);
      return _userFromFirebaseUser(user);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
        return null;
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
        return 'email-already-in-use';
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  // send verification mail to user
  Future<void> verifyUser(User user) async {
    if (!user.emailVerified) {
      await user.sendEmailVerification();
    }
  }

  // logout user
  Future<void> logout() async {
    try {
      await _firebaseAuth.signOut();
    } on FirebaseAuthException catch (e) {
      print(e.message);
    }
  }
}