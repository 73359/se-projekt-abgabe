import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_gamifit_frontend/models/exercise.dart';
import 'package:flutter_gamifit_frontend/models/user.dart';
import 'package:flutter_gamifit_frontend/models/training.dart';

// services for database
class DatabaseService {
  final String uid;

  DatabaseService({this.uid});

  //collection reference
  final CollectionReference usersCollection =
      FirebaseFirestore.instance.collection('GamiFitUsers');
  final CollectionReference trainingsCollection =
      FirebaseFirestore.instance.collection('GamiFitTrainings');
  final CollectionReference exerciseCollection =
      FirebaseFirestore.instance.collection('GamiFitExercise');

  // save user data in firestore
  Future<void> updateUserData(String userName, int age, double weight,
      int experience, bool hearts, trainings) {
    // Call the user's CollectionReference to add a new user
    return usersCollection
        .doc(uid)
        .set({
          'uid': uid,
          'username': userName, // gamifituser
          'age': age, // 0
          'weight': weight, // 0.0
          'experience': experience,
          'hearts': hearts,
          'addedTrainings': trainings,
        })
        .then((value) => print("User Data Added/Updated"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  // List of user data from Firestore user collection
  UserData _userDataFromSnapshot(DocumentSnapshot doc) {
    return UserData(
      uid: uid,
      username: doc.get("username") ?? '-',
      age: doc.get("age") ?? 0,
      weight: doc.get("weight") ?? 0.0,
      experience: doc.get("experience") ?? 0,
      hearts: doc.get("hearts") ?? false,
      addedTrainings: doc.get("addedTrainings") ?? [],
    );
  }

  // Stream to get user data from Firestore user collection
  Stream<UserData> get userData {
    try {
      return usersCollection.doc(uid).snapshots().map(_userDataFromSnapshot);
    } on StateError catch (e) {
      print('No user data found in database! ' + e.message);
      return null;
    }
  }

  // List of Trainings from Firestore trainings collection
  List<Training> _trainingListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs
        .map((doc) => Training(
              name: doc.get("name") ?? '',
              type: doc.get("type") ?? '',
              intensity: doc.get("intensity") ?? 0,
              exerciseList: doc.get("exerciseList") ?? [],
            ))
        .toList();
  }

  // Stream to get trainings data from Firestore trainings collection
  Stream<List<Training>> get allTrainings {
    try {
      return trainingsCollection.snapshots().map(_trainingListFromSnapshot);
    } on StateError catch (e) {
      print('No trainings in database! ' + e.message);
      return null;
    }
  }

  //Stream to get added trainings from user addedTrainings docReferences
  Stream<List<Training>> userAddedTrainings(List<dynamic> docRefs) {
    try {
      if (docRefs.isEmpty) {
        return null;
      }
      return trainingsCollection
          .where(FieldPath.documentId, whereIn: docRefs)
          .snapshots()
          .map(_trainingListFromSnapshot);
    } on StateError catch (e) {
      print('No trainings added to user in database! ' + e.message);
      return null;
    }
  }

  // List of Exercises in a Training
  List<Exercise> _exerciseListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs
        .map((doc) => Exercise(
              name: doc.get("name") ?? '',
              repetitions: doc.get("repetitions") ?? 0,
              sets: doc.get("sets") ?? 0,
            ))
        .toList();
  }

  // Stream to get Exercises added inside a Training
  Stream<List<Exercise>> exercisesInTraining(List<dynamic> docRefs) {
    try {
      return exerciseCollection
          .where(FieldPath.documentId, whereIn: docRefs)
          .snapshots()
          .map(_exerciseListFromSnapshot);
    } on StateError catch (e) {
      print('No exercises added to training in database! ' + e.message);
      return null;
    }
  }
  // List of user data from Firestore user collection
  List<UserData> _userListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs
        .map((doc) => UserData(
      uid: doc.data()['uid'] ?? '-',
      username: doc.data()['username'] ?? '-',
      age: doc.data()["age"] ?? 0,
      weight: doc.data()["weight"] ?? 0.0,
      experience: doc.data()["experience"] ?? 0,
      hearts: doc.data()["hearts"] ?? false,
      addedTrainings: doc.data()["addedTrainings"] ?? [],
    ))
        .toList();
  }

  // Stream to get all gamifit users from Firestore for Leaderboard
  Stream<List<UserData>> get gamiFitUsers {
    try {
      return usersCollection
          .orderBy('experience', descending: true)
          .snapshots()
          .map(_userListFromSnapshot);
    } on StateError catch (e) {
      print('No Gamifit Users found in database! ' + e.message);
      return null;
    }
  }



}
