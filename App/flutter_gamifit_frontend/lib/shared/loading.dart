import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

// Loading Widget
class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(20), color: Colors.orangeAccent,),
      margin: EdgeInsets.all(20),
      child: Center(
        child: SpinKitChasingDots(
          color: Colors.orangeAccent[100],
          size: 50,
        ),
      ),
    );
  }
}