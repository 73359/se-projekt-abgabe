import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/models/exercise.dart';
import 'package:flutter_gamifit_frontend/models/user.dart';
import 'package:flutter_gamifit_frontend/models/training.dart';
import 'package:flutter_gamifit_frontend/services/database.dart';

final Color buttonColor = Colors.orangeAccent;
final Color inActiveBarColor = Colors.grey;
final Color scoreIconColor = Colors.black;
final Color scoreCountColor = Colors.grey;

final double scoreIconSize = 30.0;
final double scoreCountSize = 15.0;

final double levelIconSize = 30.0;

final double topEdgeInset = 115.0;
final double leftAndRightEdgeInsets = 15.0;

final Color exerciseIconColor = Colors.black;
final double exerciseIconSize = 60.0;

final Color exerciseDoneColor = Colors.greenAccent[50];
final Color exercise75Color = Colors.greenAccent[100];
final Color exercise50Color = Colors.greenAccent;
final Color exercise25Color = Colors.greenAccent[400];
final Color exerciseTodoColor = Colors.greenAccent[700];

final double topBarHeight = 90;

// decoration of TestInputs, can be extended in code with ".copyWith()"
const textInputDecoration = InputDecoration(
  fillColor: Colors.white,
  filled: true,
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.white, width: 2.0),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.orangeAccent, width: 2.0),
  ),
);

// update user experience after perform exercise or do learnings
void updateUserExperience(UserData userData, int experience) async {
  await DatabaseService(uid: userData.uid)
      .updateUserData(
      userData.username,
      userData.age,
      userData.weight,
      userData.experience + experience,
      userData.hearts,
      userData.addedTrainings
  );
}

// determine the asset image of viewed Training
String determineCurrentTrainingType(Training training) {
  String trainingsIcon;
  switch (training.type) {
    case 'upper':
      trainingsIcon = 'assets/icons/training/icons8-torso-100.png';
      break;
    case 'under':
      trainingsIcon = 'assets/icons/training/icons8-hamstrings-100.png';
      break;
    case 'fullbody':
      trainingsIcon = 'assets/icons/training/icons8-body-100.png';
      break;
    default:
      trainingsIcon = 'assets/icons/tabbar/icons8-dumbbell-100.png';
      break;
  }
  return trainingsIcon;
}

// determine the asset image of viewed Exercise
String determineCurrentExercise(Exercise exercise) {
  String exerciseIcon;
  switch (exercise.name) {
    case 'calves':
      exerciseIcon = 'assets/icons/training/icons8-calves-100.png';
      break;
    case 'legPress':
      exerciseIcon = 'assets/icons/training/icons8-quadriceps-100.png';
      break;
    case 'latPull':
      exerciseIcon = 'assets/icons/training/icons8-bodybuilder-100.png';
      break;
    case 'benchPress':
      exerciseIcon = 'assets/icons/training/icons8-brust-100.png';
      break;
    case 'dips':
      exerciseIcon = 'assets/icons/training/icons8-trizeps-100.png';
      break;
    case 'militaryPress':
      exerciseIcon = 'assets/icons/training/icons8-schultern-100.png';
      break;
    default:
      exerciseIcon = 'assets/icons/tabbar/icons8-dumbbell-100.png';
      break;
  }
  return exerciseIcon;
}