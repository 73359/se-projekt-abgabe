import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_gamifit_frontend/screens/authenticate/authenticate.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gamifit_frontend/screens/home/gamifit_home.dart';


class Wrapper extends StatelessWidget {
  const Wrapper({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final firebaseUser = Provider.of<User>(context);

    //checks if user is logged in && if user's email is verified
    if(firebaseUser != null && firebaseUser.emailVerified) {
      return GamiFitHome();
    }
    return Authenticate();
  }
}
