import 'package:cloud_firestore/cloud_firestore.dart';

// gmftUser model for login
class GMFTUser {
  final String uid;
  final bool emailVerified;

  GMFTUser({ this.uid, this.emailVerified });
}

// UserData model for mapping firebase user data snapshots
class UserData {
  final String uid;
  final String username;
  final int age;
  final double weight;
  final int experience; //level = experience / 1000
  final bool hearts; //life for Trainings

  List<dynamic> addedTrainings = [];

  UserData({ this.uid, this.username, this.age, this.weight, this.hearts, this.experience, this.addedTrainings });
}