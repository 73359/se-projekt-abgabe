// Training model for mapping firebase training snapshots
class Training {

  final String name;
  final String type;
  final int intensity;
  var exerciseList = [];

  Training({ this.name, this.type, this.intensity, this.exerciseList });

}