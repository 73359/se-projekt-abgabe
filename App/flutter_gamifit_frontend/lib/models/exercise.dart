// Exercise model for mapping firebase exercise snapshots
class Exercise {

  final String name;
  final int sets;
  final int repetitions;

  Exercise({ this.name, this.sets, this.repetitions });
}