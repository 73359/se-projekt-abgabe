import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gamifit_frontend/models/user.dart';
import 'package:flutter_gamifit_frontend/services/database.dart';
import 'package:flutter_gamifit_frontend/shared/loading.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gamifit_frontend/shared/globals.dart';
import 'package:flutter_gamifit_frontend/services/auth_service.dart';

class ProfileTab extends StatefulWidget {
  @override
  _ProfileTabState createState() => _ProfileTabState();
}

class _ProfileTabState extends State<ProfileTab> {
  final _formKey = GlobalKey<FormState>();

  String _username;
  String _age;
  String _weight;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    //Stream builder for streaming user data in leaderboard
    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userData,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return new Loading();
        } else {
          UserData userData = snapshot.data;
          return Scaffold(
            backgroundColor: Colors.orangeAccent,
            body: Form(
              key: _formKey,
              child: Container(
                margin: EdgeInsets.only(
                  top: topEdgeInset,
                  left: leftAndRightEdgeInsets,
                  right: leftAndRightEdgeInsets,
                ),
                child: SingleChildScrollView(
                  physics: NeverScrollableScrollPhysics(),
                  child: Column(
                    children: [
                      Text(
                        'Update your GamiFit-Account',
                        style: TextStyle(
                            fontSize: 20.0, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Change your username:',
                        style: TextStyle(
                            fontSize: 15.0, fontWeight: FontWeight.bold),
                      ),
                      TextFormField(
                        decoration: textInputDecoration.copyWith(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.red, width: 2.0),
                            ),
                            hintText: userData.username),
                        //validator: (val) => val.isEmpty ? 'Please enter a username' : null,
                        onChanged: (val) => setState(() => _username = val),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Change your age:',
                        style: TextStyle(
                            fontSize: 15.0, fontWeight: FontWeight.bold),
                      ),
                      TextFormField(
                        decoration: textInputDecoration.copyWith(
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.red, width: 2.0),
                          ),
                          hintText: userData.age.toString() + ' years',
                          counterText: "",
                        ),
                        //validator: (val) => val.isEmpty ? 'Please enter your age' : null,
                        onChanged: (val) => setState(() => _age = val),
                        maxLength: 2,
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        'Change your weight:',
                        style: TextStyle(
                            fontSize: 15.0, fontWeight: FontWeight.bold),
                      ),
                      TextFormField(
                        decoration: textInputDecoration.copyWith(
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.red, width: 2.0),
                          ),
                          hintText: userData.weight.toString() + ' kg',
                          counterText: "",
                        ),
                        //validator: (val) => val.isEmpty ? 'Please enter your weight' : null,
                        onChanged: (val) => setState(() => _weight = val),
                        maxLength: 4,
                        keyboardType: TextInputType.number,
                        /*inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.digitsOnly
                        ],*/
                      ),
                      SizedBox(
                        height: 100,
                      ),
                      ElevatedButton(
                          child: Text(
                            'Update',
                            style: TextStyle(color: Colors.white),
                          ),
                          style: ElevatedButton.styleFrom(
                            primary: Colors.orange,
                          ),
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              await DatabaseService(uid: user.uid)
                                  .updateUserData(
                                      _username ?? userData.username,
                                      _age != null
                                          ? int.parse(_age)
                                          : userData.age,
                                      _weight != null
                                          ? double.parse(_weight)
                                          : userData.weight,
                                      userData.experience,
                                      userData.hearts,
                                      userData.addedTrainings);
                              Navigator.of(context).pop();
                            }
                          }),
                    ],
                  ),
                ),
              ),
            ),
            floatingActionButton: FloatingActionButton(
              child: Icon(
                Icons.logout,
              ),
              backgroundColor: Colors.black,
              onPressed: () {
                context.read<AuthService>().logout();
              },
            ),
          );
        }
      },
    );
  }
}
