import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/models/user.dart';
import 'package:flutter_gamifit_frontend/services/database.dart';
import 'package:flutter_gamifit_frontend/shared/globals.dart';
import 'package:flutter_gamifit_frontend/shared/loading.dart';
import 'package:provider/provider.dart';

class ScoreMenuBar extends StatelessWidget {
  int userLevel;
  int userExperience;
  double levelProgress;
  String currentLevelIcon;
  String nextLevelIcon;
  bool userHeart;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userData,
      initialData: UserData(uid: user.uid, experience: 0, hearts: false),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return new Loading();
        UserData userData = snapshot.data;
        userExperience = userData.experience;
        userHeart = userData.hearts;
        calculateUserLevel(userData, userExperience);
        decideOnLevelIcons();
        return Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(30),
                      bottomRight: Radius.circular(30))),
              height: 100,
            ),
            Container(
              padding: EdgeInsets.only(left: 25, right: 25),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 25,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Row(
                        children: [
                          ImageIcon(
                            AssetImage(
                                'assets/icons/scoreBar/icons8-crown-100.png'),
                            color: Colors.greenAccent,
                            size: scoreIconSize,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            userData != null ? '$userExperience' : '-',
                            style: TextStyle(
                                color: scoreCountColor,
                                fontSize: scoreCountSize),
                          ),
                        ],
                      ),
                      SizedBox(
                        width: 25,
                      ),
                      Row(
                        children: [
                          ImageIcon(
                            AssetImage(
                                'assets/icons/scoreBar/icons8-heart-100.png'),
                            color: userHeart ? Colors.red : Colors.grey,
                            size: scoreIconSize,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                        ],
                      ),
                    ],
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      ImageIcon(
                        AssetImage(currentLevelIcon),
                        color: Colors.greenAccent,
                        size: levelIconSize,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Expanded(
                        child: LinearProgressIndicator(
                          minHeight: 10,
                          value: levelProgress,
                          backgroundColor: Colors.orange[100],
                          valueColor: AlwaysStoppedAnimation<Color>(
                              Colors.orangeAccent),
                        ),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      ImageIcon(
                        AssetImage(nextLevelIcon),
                        color: Colors.grey,
                        size: levelIconSize,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                left: MediaQuery.of(context).size.width / 2 - 15,
                top: 29
              ),
              child: InkWell(
                child: ImageIcon(
                  AssetImage('assets/icons/scoreBar/icons8-name-100.png'),
                  color: Colors.orangeAccent,
                  size: scoreIconSize,
                ),
                onTap: () {
                  Navigator.pushNamed(context, '/profile');
                },
              ),
            ),
          ],
        );
      },
    );
  }

  void calculateUserLevel(UserData userData, int userExperience) {
    if (userExperience < 1000) {
      userLevel = 0;
      levelProgress = userExperience / 1000;
    } else {
      userLevel = (userExperience / 1000).floor();
      levelProgress = (userExperience - (userLevel * 1000)) / 1000;
    }
  }

  void decideOnLevelIcons() {
    switch (userLevel) {
      case 0:
        currentLevelIcon =
            'assets/icons/level/filled/icons8-circled-0-c-96.png';
        nextLevelIcon = 'assets/icons/level/unfilled/icons8-circled-1-96.png';
        break;
      case 1:
        currentLevelIcon =
            'assets/icons/level/filled/icons8-circled-1-c-96.png';
        nextLevelIcon = 'assets/icons/level/unfilled/icons8-circled-2-96.png';
        break;
      case 2:
        currentLevelIcon =
            'assets/icons/level/filled/icons8-circled-2-c-96.png';
        nextLevelIcon = 'assets/icons/level/unfilled/icons8-circled-3-96.png';
        break;
      case 3:
        currentLevelIcon =
            'assets/icons/level/filled/icons8-circled-3-c-96.png';
        nextLevelIcon = 'assets/icons/level/unfilled/icons8-circled-4-96.png';
        break;
      case 4:
        currentLevelIcon =
            'assets/icons/level/filled/icons8-circled-4-c-96.png';
        nextLevelIcon = 'assets/icons/level/unfilled/icons8-circled-5-96.png';
        break;
      case 5:
        currentLevelIcon =
            'assets/icons/level/filled/icons8-circled-5-c-96.png';
        nextLevelIcon = 'assets/icons/level/unfilled/icons8-circled-6-96.png';
        break;
      case 6:
        currentLevelIcon =
            'assets/icons/level/filled/icons8-circled-6-c-96.png';
        nextLevelIcon = 'assets/icons/level/unfilled/icons8-circled-7-96.png';
        break;
      case 7:
        currentLevelIcon =
            'assets/icons/level/filled/icons8-circled-7-c-96.png';
        nextLevelIcon = 'assets/icons/level/unfilled/icons8-circled-8-96.png';
        break;
      case 8:
        currentLevelIcon =
            'assets/icons/level/filled/icons8-circled-8-c-96.png';
        nextLevelIcon = 'assets/icons/level/unfilled/icons8-circled-9-96.png';
        break;
      case 9:
        currentLevelIcon =
            'assets/icons/level/filled/icons8-circled-9-c-96.png';
        nextLevelIcon = 'assets/icons/level/unfilled/icons8-circled-0-96.png';
        break;
      default:
        currentLevelIcon =
            'assets/icons/level/filled/icons8-circled-0-c-96.png';
        nextLevelIcon = 'assets/icons/level/unfilled/icons8-circled-1-96.png';
    }
  }
}
