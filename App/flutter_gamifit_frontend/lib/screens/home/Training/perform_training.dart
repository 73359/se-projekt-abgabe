import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/models/exercise.dart';
import 'package:flutter_gamifit_frontend/models/training.dart';
import 'package:flutter_gamifit_frontend/screens/home/Training/training_exercise_lists/exercise_grid.dart';
import 'package:flutter_gamifit_frontend/screens/home/home_menu/scores_menu_bar.dart';
import 'package:flutter_gamifit_frontend/services/database.dart';
import 'package:flutter_gamifit_frontend/shared/globals.dart';
import 'package:provider/provider.dart';

// performing a training
class PerformTraining extends StatelessWidget {
  final Training performedTraining;

  PerformTraining({ this.performedTraining });

  @override
  Widget build(BuildContext context) {
    return StreamProvider<List<Exercise>>.value(
      initialData: [],
      value: DatabaseService().exercisesInTraining(performedTraining.exerciseList),
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Container(
              margin:
              EdgeInsets.only(top: 105, left: 20, right: 20, bottom: 20),
              padding: EdgeInsets.only(top: 25, bottom: 20),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.black,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(child: SizedBox()),
                      Container(
                        child: Text(
                          'Perform Your Exercises!',
                          style: TextStyle(
                            color: Colors.orangeAccent,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Expanded(child: SizedBox()),
                    ],
                  ),
                  // List of Exercises in Training on Database
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(bottom: 20),
                      padding: EdgeInsets.only(top: 25),
                      child: ExerciseGrid(),
                    ),
                  ),
                  InkWell(
                    child: Container(
                      alignment: Alignment.center,
                      height: 65,
                      width: 250,
                      decoration: BoxDecoration(
                          color: buttonColor, borderRadius: BorderRadius.circular(10)),
                      child:
                          Text('Finish', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                    ),
                    onTap: () {
                      Navigator.pop(context); //TODO
                    },
                  ),
                ],
              ),
            ),
            ScoreMenuBar(),
          ],
        ),
      ),
    );
  }
}
