import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/screens/home/Training/widgets/new_training_tile.dart';
import 'package:flutter_gamifit_frontend/models/training.dart';
import 'package:flutter_gamifit_frontend/shared/loading.dart';
import 'package:provider/provider.dart';

// ListView for all Trainings in database
class NewTrainingsList extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    List<Training> trainingsList = Provider.of<List<Training>>(context);
    return (trainingsList != null) ? ListView.builder(
        itemCount: trainingsList.length,
        itemBuilder: (BuildContext context, int index) {
          return NewTrainingTile(training: trainingsList[index]);
        })
    // if no trainings found in database -> Loading widget
        : Loading();
  }
}

