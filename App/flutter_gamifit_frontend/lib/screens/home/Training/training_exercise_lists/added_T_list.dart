import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/models/user.dart';
import 'package:flutter_gamifit_frontend/models/training.dart';
import 'package:flutter_gamifit_frontend/screens/home/Training/widgets/added_training_tile.dart';
import 'package:flutter_gamifit_frontend/services/database.dart';
import 'package:flutter_gamifit_frontend/shared/loading.dart';
import 'package:provider/provider.dart';

//ListView for all added Trainings
class AddedTrainingsList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // provides User data
    UserData userData = Provider.of<UserData>(context);
    // streams added Trainings of user
    return StreamBuilder <List <Training>>(
        stream: DatabaseService(uid: userData.uid).userAddedTrainings(
            userData.addedTrainings ?? []),
        builder: (context, snapshot) {
          // no Trainings added ?
          if (!snapshot.hasData) return Loading();
          // Trainings added -> show
          List<Training> addedTrainingsList = snapshot.data;
          return ListView.builder(
              itemCount: addedTrainingsList.length,
              itemBuilder: (BuildContext context, index) {
                return AddedTrainingTile(training: addedTrainingsList[index]);
              });
        });
  }
}
