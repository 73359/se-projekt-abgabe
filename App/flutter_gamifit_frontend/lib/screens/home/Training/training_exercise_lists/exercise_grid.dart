import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/models/exercise.dart';
import 'package:flutter_gamifit_frontend/screens/home/Training/widgets/exercise_tile.dart';
import 'package:flutter_gamifit_frontend/shared/loading.dart';
import 'package:provider/provider.dart';

// ListView for all Trainings in database
class ExerciseGrid extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    List<Exercise> exercises = Provider.of<List<Exercise>>(context);
    return (exercises != null)
        ? GridView.builder(
            padding: EdgeInsets.all(10),
            itemCount: exercises.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 10,
              crossAxisSpacing: 10,
            ),
            itemBuilder: (BuildContext context, int index) {
              return new ExerciseTile(
                exercise: exercises[index],
                exerciseNumber: index + 1
              );
            },
          )
        //if no exercises added -> Loading Widget
        : Loading();
  }
}
