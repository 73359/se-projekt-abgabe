import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/models/user.dart';
import 'package:flutter_gamifit_frontend/models/training.dart';
import 'package:flutter_gamifit_frontend/services/database.dart';
import 'package:flutter_gamifit_frontend/shared/loading.dart';
import 'package:provider/provider.dart';
import '../../../../shared/globals.dart';
import '../perform_training.dart';

// single trainings tile for al added Trainings of user
class AddedTrainingTile extends StatelessWidget {
  final Training training;

  AddedTrainingTile({this.training});

  DocumentReference trainingsReference;

  String trainingsIcon;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    trainingsIcon = determineCurrentTrainingType(training);
    return StreamBuilder<UserData>(
        stream: DatabaseService(uid: user.uid).userData,
        builder: (context, snapshot) {
          if (!snapshot.hasData) return new Loading();
          UserData userData = snapshot.data;
          trainingsReference = FirebaseFirestore.instance
              .collection('GamiFitTrainings')
              .doc(training.name);
          return InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        PerformTraining(performedTraining: training)),
              );
            },
            child: Container(
              height: 100,
              margin: EdgeInsets.only(
                left: leftAndRightEdgeInsets,
                right: leftAndRightEdgeInsets,
                bottom: 10,
              ),
              padding:
                  EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 10),
              decoration: BoxDecoration(
                color: buttonColor,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  new BoxShadow(
                      color: Colors.grey.withOpacity(0.5), blurRadius: 10.0),
                ],
              ),
              child: Row(
                children: <Widget>[
                  ImageIcon(
                    AssetImage(trainingsIcon),
                    color: scoreIconColor,
                    size: scoreIconSize,
                  ),
                  SizedBox(
                    width: 25,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        training.name,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 2,),
                      Text(
                        '${training.exerciseList.length} Exercises',
                        style: TextStyle(color: Colors.black, fontSize: 10),
                      ),
                    ],
                  ),
                  Expanded(child: SizedBox(),),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        training.type == 'under'
                            ? 'lower body'
                            : training.type == 'upper'
                                ? 'upper body'
                                : 'full body',
                        style: TextStyle(color: Colors.black, fontSize: 10),
                      ),
                      training.intensity >= 4
                          ? Text('HARD',
                              style: TextStyle(
                                  color: Colors.red[800], fontSize: 15))
                          : training.intensity == 3
                              ? Text('MEDIUM',
                                  style: TextStyle(
                                      color: Colors.orange[800], fontSize: 15))
                              : Text('LOW',
                                  style: TextStyle(
                                      color: Colors.green, fontSize: 15))
                    ],
                  ),
                  SizedBox(width: 10,),
                  IconButton(
                      icon: Icon(
                        Icons.close,
                        size: 35,
                      ),
                      onPressed: () {
                        createNotificationDialog(context, userData);
                      }),
                ],
              ),
            ),
          );
        });
  }

  createNotificationDialog(BuildContext context, UserData userData) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Remove Training:'),
            content: Text(
                'Do you want to remove ${training.name} from your added trainings?'),
            actions: [
              Row(
                children: [
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.redAccent[100],
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Icon(
                        Icons.close,
                        color: Colors.redAccent[700],
                      )),
                  SizedBox(
                    width: 10,
                  ),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.greenAccent[100],
                      ),
                      onPressed: () async {
                        userData.addedTrainings.remove(trainingsReference);
                        await DatabaseService(uid: userData.uid).updateUserData(
                            userData.username,
                            userData.age,
                            userData.weight,
                            userData.experience,
                            userData.hearts,
                            userData.addedTrainings);
                        Navigator.of(context).pop();
                      },
                      child: Icon(
                        Icons.done,
                        color: Colors.greenAccent[700],
                      )),
                ],
              ),
            ],
          );
        });
  }
}
