import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/models/exercise.dart';
import 'package:flutter_gamifit_frontend/models/user.dart';
import 'package:flutter_gamifit_frontend/services/database.dart';
import 'package:flutter_gamifit_frontend/shared/globals.dart';
import 'package:flutter_gamifit_frontend/shared/loading.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';

class ExerciseTile extends StatefulWidget {
  final Exercise exercise;
  final int exerciseNumber;

  ExerciseTile({this.exercise, this.exerciseNumber});

  @override
  _ExerciseTileState createState() => _ExerciseTileState();
}

class _ExerciseTileState extends State<ExerciseTile> {
  bool exerciseDone = false;
  int _reps = 0;
  int _sets = 0;
  int experience = 0;

  String exerciseIcon;

  @override
  void initState() {
    super.initState();
    setState(() {
      _reps = widget.exercise.repetitions;
      _sets = widget.exercise.sets;
    });
  }

  @override
  Widget build(BuildContext context) {
    exerciseIcon = determineCurrentExercise(widget.exercise);
    final user = Provider.of<User>(context);
    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userData,
      builder: (context, snapshot) {
        if (!snapshot.hasData) return new Loading();
        UserData userData = snapshot.data;
        return GridTile(
          child: new InkResponse(
            enableFeedback: true,
            //per set 5 XP and additional 5 XP for finishing all sets of an exercise.
            onTap: () {
              if (_sets > 1) {
                setState(() {
                  _sets -= 1;
                  experience = 5;
                  updateUserExperience(userData, experience);
                });
              } else if (_sets == 1) {
                setState(() {
                  _sets -= 1;
                  experience = 10;
                  exerciseDone = true;
                  updateUserExperience(userData, experience);
                });
              }
            },
            child: Container(
              decoration: BoxDecoration(
                color: _sets == 4
                    ? exerciseTodoColor
                    : _sets == 3
                        ? exercise25Color
                        : _sets == 2
                            ? exercise50Color
                            : _sets == 1
                                ? exercise75Color
                                : _sets == 0
                                    ? exerciseDoneColor
                                    : Colors.black,
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  new BoxShadow(
                      color: Colors.grey.withOpacity(0.5), blurRadius: 10.0),
                ],
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 5, top: 5),
                        child: Text(
                          '${widget.exerciseNumber}',
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ImageIcon(
                        AssetImage(exerciseIcon),
                        color: exerciseIconColor,
                        size: exerciseIconSize,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            widget.exercise.name,
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          ),
                          Text(
                            '$_sets' + ' * ' + '$_reps',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.w900),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
