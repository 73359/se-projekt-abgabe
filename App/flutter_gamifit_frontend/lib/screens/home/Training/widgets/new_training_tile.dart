import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/models/user.dart';
import 'package:flutter_gamifit_frontend/services/database.dart';
import 'package:flutter_gamifit_frontend/shared/globals.dart';
import 'package:flutter_gamifit_frontend/models/training.dart';
import 'package:flutter_gamifit_frontend/shared/loading.dart';
import 'package:provider/provider.dart';

// single trainings tile for all trainings in database
class NewTrainingTile extends StatefulWidget {
  final Training training;

  NewTrainingTile({this.training});

  @override
  _NewTrainingTileState createState() => _NewTrainingTileState();
}

class _NewTrainingTileState extends State<NewTrainingTile> {
  String trainingsIcon;

  List<dynamic> newUserTrainingsList;

  DocumentReference trainingsReference;

  bool alreadyAdded = false;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    trainingsIcon = determineCurrentTrainingType(widget.training);
    return StreamBuilder<UserData>(
        stream: DatabaseService(uid: user.uid).userData,
        builder: (context, snapshot) {
          if (!snapshot.hasData) return new Loading();
          UserData userData = snapshot.data;
          trainingsReference = FirebaseFirestore.instance.collection('GamiFitTrainings').doc(widget.training.name);
          return Padding(
            padding: EdgeInsets.fromLTRB(10.0, 8.0, 0.0, 0.0),
            child: Card(
              color: buttonColor,
              margin: EdgeInsets.only(left: 15, right: 15, bottom: 10),
              child: ListTile(
                leading: ImageIcon(
                  AssetImage(trainingsIcon),
                  color: scoreIconColor,
                  size: scoreIconSize,
                ),
                onTap: () async {
                  if (userData.addedTrainings.contains(trainingsReference)) {
                    setState(() {
                      alreadyAdded = true;
                    });
                  } else {
                    userData.addedTrainings.add(trainingsReference);
                    print(userData.addedTrainings);
                    await DatabaseService(uid: user.uid).updateUserData(
                        userData.username,
                        userData.age,
                        userData.weight,
                        userData.experience,
                        userData.hearts,
                        userData.addedTrainings);
                    Navigator.of(context).pop();
                  }
                },
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.training.name,
                          style: TextStyle(color: Colors.black, fontSize: 20),
                        ),
                        Text(
                          '${widget.training.exerciseList.length} Exercises',
                          style: TextStyle(color: Colors.black, fontSize: 10),
                        ),
                        alreadyAdded ? Text('Already added!', style: TextStyle(color: Colors.red, fontSize: 10),) : Text(''),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(widget.training.type == 'under' ? 'lower body' : widget.training.type == 'upper' ? 'upper body' : 'full body',
                          style: TextStyle(color: Colors.black, fontSize: 10),
                        ),
                        widget.training.intensity >= 4 ? Text('HARD',
                            style: TextStyle(color: Colors.red[800], fontSize: 15)) :
                        widget.training.intensity == 3 ? Text('MEDIUM',
                            style: TextStyle(color: Colors.orange[800], fontSize: 15)) :
                        Text('LOW', style: TextStyle(color: Colors.green, fontSize: 15))
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        }
      );
  }
}
