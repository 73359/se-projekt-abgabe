import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/shared/globals.dart';
import 'package:flutter_gamifit_frontend/models/user.dart';
import 'package:flutter_gamifit_frontend/screens/home/Training/training_exercise_lists/added_T_list.dart';
import 'package:flutter_gamifit_frontend/services/database.dart';
import 'package:provider/provider.dart';
import 'new_trainings_plan.dart';

//Training tab
class BottomTabBarTraining extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // to get userID
    final user = Provider.of<User>(context);
    //Stream provider for providing user data
    return StreamProvider<UserData>.value(
      catchError: (_, err) => UserData(uid: user.uid),
      value: DatabaseService(uid: user.uid).userData,
      initialData: UserData(uid: user.uid),
      child: Column(
        children: [
          Expanded(
            child: Container(
              color: Colors.white,
              margin: EdgeInsets.only(bottom: 10, top: topEdgeInset),
              // List of added Trainings of user
              child: AddedTrainingsList(),
            ),
          ),
          Container(
            color: Colors.white,
            height: 150,
            width: 80,
            // Add Button navigates to add new trainings
            child: FloatingActionButton(
              child: Icon(
                Icons.add,
                size: 70,
              ),
              backgroundColor: buttonColor,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => NewTrainingsPlan()),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
