import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/screens/home/Training/training_exercise_lists/new_training_list.dart';
import 'package:flutter_gamifit_frontend/screens/home/home_menu/scores_menu_bar.dart';
import 'package:flutter_gamifit_frontend/services/database.dart';
import 'package:provider/provider.dart';
import 'package:flutter_gamifit_frontend/models/training.dart';
import '../../../shared/globals.dart';

// add new Training tab
class NewTrainingsPlan extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<List<Training>>.value(
      initialData: [],
      value: DatabaseService().allTrainings,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Container(
              margin:
                  EdgeInsets.only(top: 105, left: 20, right: 20, bottom: 20),
              padding: EdgeInsets.only(top: 25, bottom: 50),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.black,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(child: SizedBox()),
                      Container(
                        child: Text(
                          'Choose A Training',
                          style: TextStyle(
                            color: Colors.orangeAccent,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Expanded(child: SizedBox()),
                    ],
                  ),
                  // List of Trainings on Database
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(bottom: 65),
                      padding: EdgeInsets.only(top: 25),
                      child: NewTrainingsList(),
                    ),
                  ),
                  newTrainingButtons(context)
                ],
              ),
            ),
            ScoreMenuBar(),
          ],
        ),
      ),
    );
  }

  // Widget for two buttons "cancel add Training" and "edit custom Training"
  Widget newTrainingButtons(context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(child: SizedBox()),
        InkWell(
          child: Container(
            height: 50,
            width: 120,
            decoration: BoxDecoration(
                color: buttonColor, borderRadius: BorderRadius.circular(10)),
            child: Column(
              children: [
                Expanded(child: SizedBox()),
                Icon(Icons.cancel),
                Expanded(child: SizedBox()),
              ],
            ),
          ),
          onTap: () {
            Navigator.pop(context);
          },
        ),
        Expanded(child: SizedBox()),
      ],
    );
  }
}
