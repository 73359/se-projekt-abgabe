import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/models/user.dart';
import 'package:flutter_gamifit_frontend/shared/globals.dart';

class LeaderboardTile extends StatelessWidget {
  final UserData userData;
  final int rang;

  LeaderboardTile({ this.userData, this.rang });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {

      },
      child: Container(
        height: 100,
        margin: EdgeInsets.only(left: leftAndRightEdgeInsets, right: leftAndRightEdgeInsets, bottom: 10, top: 0),
        padding: EdgeInsets.only(left: 20),
        decoration: BoxDecoration(
          color: buttonColor,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            new BoxShadow(
                color: Colors.grey.withOpacity(0.5), blurRadius: 10.0),
          ],
        ),
        child: Row(
          children: <Widget>[
            Text(rang.toString(),
              style: TextStyle(
                color: Colors.black,
                fontSize: 30,
                fontWeight: FontWeight.bold,
              ),
            ),
            /*ImageIcon(
              AssetImage(userData.pro),
              color: scoreIconColor,
              size: scoreIconSize,
            ),*/
            SizedBox(
              width: 25,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  userData.username,
                  style: TextStyle(color: Colors.black, fontSize: 20),
                ),
                Text('XP: ' +
                  userData.experience.toString(),
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
