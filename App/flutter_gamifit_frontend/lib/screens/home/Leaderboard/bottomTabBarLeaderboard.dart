import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/models/user.dart';
import 'package:flutter_gamifit_frontend/services/database.dart';
import 'package:provider/provider.dart';
import 'leaderboard_list.dart';

class BottomTabBarLeaderboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<List<UserData>>.value(
      catchError: (_, err) => [],
      value: DatabaseService().gamiFitUsers,
      initialData: [],
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            Container(
              margin:
                  EdgeInsets.only(top: 105, left: 20, right: 20, bottom: 20),
              padding: EdgeInsets.only(top: 25, bottom: 50),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.black,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(child: SizedBox()),
                      Container(
                        child: Text(
                          'Leaderboard',
                          style: TextStyle(
                            color: Colors.orangeAccent,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Expanded(child: SizedBox()),
                    ],
                  ),
                  // List of Trainings on Database
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(bottom: 65),
                      padding: EdgeInsets.only(top: 25),
                      child: LeaderboardList(),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
