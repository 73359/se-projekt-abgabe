import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/models/user.dart';
import 'package:flutter_gamifit_frontend/shared/loading.dart';
import 'package:provider/provider.dart';
import 'leaderboard_tile.dart';

class LeaderboardList extends StatelessWidget {
  const LeaderboardList({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<UserData> gamifitUserList = Provider.of<List<UserData>>(context) ?? [];
    return (gamifitUserList != null) ? ListView.builder(
        itemCount: gamifitUserList.length,
        itemBuilder: (BuildContext context, int index) {
          return LeaderboardTile(userData: gamifitUserList[index], rang: index+1);
        })
    // if no trainings found in database -> Loading widget
        : Loading();
  }
}
