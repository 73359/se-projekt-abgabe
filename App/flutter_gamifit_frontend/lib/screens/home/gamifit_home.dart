import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/screens/home/Training/new_trainings_plan.dart';
import 'package:flutter_gamifit_frontend/screens/home/home_menu/bottomTabBarBrain.dart';
import 'package:flutter_gamifit_frontend/screens/home/Training/bottomTabBarTraining.dart';
import 'package:flutter_gamifit_frontend/screens/home/Profile/ProfileTab.dart';
import 'package:flutter_gamifit_frontend/screens/home/home_menu/scores_menu_bar.dart';
import '../../shared/globals.dart';
import 'Leaderboard/bottomTabBarLeaderboard.dart';

class GamiFitHome extends StatefulWidget {
  @override
  _GamiFitHomeState createState() => _GamiFitHomeState();
}

//todo showModalBottomSheet
class _GamiFitHomeState extends State<GamiFitHome> {
  int tabIndex = 1;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/main': (context) => GamiFitHome(),
        '/profile': (context) => ProfileTab(),
        '/newTraining': (context) => NewTrainingsPlan(),
      },
      // debug label deactivated
      debugShowCheckedModeBanner: false,
      home: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Stack(
            children: [
              // switcher between 3 tabs in changing tabindex
              Container(
                child: tabIndex == 0
                    ? BottomTabBarBrain()
                    : tabIndex == 1
                        ? BottomTabBarTraining()
                        : BottomTabBarLeaderboard(),
              ),
              ScoreMenuBar(),
            ],
          ),
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Colors.black,
            // bottom tab bar brain training & profile
            items: <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                label: '',
                icon: new Image.asset(
                  'assets/icons/tabbar/icons8-brain-100.png',
                  color: inActiveBarColor,
                  scale: 3,
                ),
                activeIcon: Image.asset(
                    'assets/icons/tabbar/icons8-brain-100.png',
                    color: buttonColor,
                    scale: 3),
              ),
              BottomNavigationBarItem(
                label: '',
                icon: new Image.asset(
                  'assets/icons/tabbar/icons8-dumbbell-100.png',
                  color: inActiveBarColor,
                  scale: 2,
                ),
                activeIcon: Image.asset(
                    'assets/icons/tabbar/icons8-dumbbell-100.png',
                    color: buttonColor,
                    scale: 2),
              ),
              BottomNavigationBarItem(
                label: '',
                icon: new Image.asset(
                  'assets/icons/tabbar/icons8-leaderboard-100.png',
                  color: inActiveBarColor,
                  scale: 2.5,
                ),
                activeIcon: Image.asset(
                    'assets/icons/tabbar/icons8-leaderboard-100.png',
                    color: buttonColor,
                    scale: 2.5),
              ),
            ],
            currentIndex: tabIndex,
            selectedItemColor: Colors.blueAccent,
            // changes tabindex to switch tab
            onTap: (int index) {
              setState(() {
                tabIndex = index;
              });
            },
          ),
        ),
      ),
    );
  }
}
