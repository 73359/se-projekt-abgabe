import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/services/auth_service.dart';
import 'package:flutter_gamifit_frontend/shared/loading.dart';
import 'package:provider/provider.dart';
import '../../shared/globals.dart';

class Login extends StatefulWidget {
  final Function toggleView;

  Login({this.toggleView});

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  // bool to change to loading screen
  bool loading = false;

  //test field state
  String email = '';
  String password = '';
  String error = '';

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            backgroundColor: Colors.black,
            resizeToAvoidBottomInset: false,
            body: Stack(
              children: [
                Container(
                  // White container
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(
                      left: 50,
                      right: 50,
                      bottom: MediaQuery.of(context).size.height * 0.4 + 20,
                      top: MediaQuery.of(context).size.height * 0.25),
                  padding: EdgeInsets.only(left: 20, right: 20),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20)),
                  child: SingleChildScrollView(
                    child: Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            SizedBox(
                              height: 50,
                            ),
                            TextFormField(
                              //Sign in email input
                              validator: (val) =>
                                  val.isEmpty ? 'Enter an email' : null,
                              onChanged: (val) {
                                setState(() {
                                  email = val;
                                });
                              },
                              controller: emailController,
                              decoration: textInputDecoration.copyWith(hintText: 'Email'),
                            ),
                            TextFormField(
                              //Sign in password Input
                              validator: (val) => val.length < 6
                                  ? 'Enter a password 6+ chars long'
                                  : null,
                              onChanged: (val) {
                                setState(() {
                                  password = val;
                                });
                              },
                              obscureText: true,
                              controller: passwordController,
                              decoration: textInputDecoration.copyWith(hintText: 'Password'),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: buttonColor,
                              ),
                              // Sign in and authenticate user
                              child: Text('Login'),
                              onPressed: () async {
                                if (_formKey.currentState.validate()) {
                                  setState(() => loading = true);
                                  dynamic result = await context
                                      .read<AuthService>()
                                      .login(
                                          email: emailController.text.trim(),
                                          password:
                                              passwordController.text.trim());
                                  if (result == null) {
                                    setState(() {
                                      error = 'No valid login credentials';
                                      loading = false;
                                    });
                                  } else if (!result.emailVerified) {
                                    setState(() {
                                      error = 'Please verify your Email';
                                      result.sendEmailVerification();
                                      loading = false;
                                    });
                                  }
                                }
                              },
                            ),
                          ],
                        )),
                  ),
                ),
                Container(
                  child: Text(
                    error,
                    style: TextStyle(color: Colors.red, fontSize: 14.0),
                  ),
                  margin: EdgeInsets.only(
                      left: 50,
                      right: 50,
                      bottom: MediaQuery.of(context).size.height * 0.4 + 20,
                      top: MediaQuery.of(context).size.height * 0.525),
                  padding: EdgeInsets.only(left: 20, right: 20),
                ),
                Container(
                  //Sign In Top Bar with Register Switch
                  height: 40,
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(
                      left: 50,
                      right: 50,
                      bottom: MediaQuery.of(context).size.height * 0.4 + 20,
                      top: MediaQuery.of(context).size.height * 0.25),
                  padding: EdgeInsets.only(left: 20, right: 20),
                  decoration: BoxDecoration(
                    color: Colors.orangeAccent,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(20),
                        topLeft: Radius.circular(20)),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Login to GamiFit',
                            style: TextStyle(fontSize: 20),
                          ),
                          InkWell(
                            onTap: () {
                              widget.toggleView();
                            },
                            child: Row(
                              children: [
                                Icon(
                                  Icons.person,
                                  size: 15,
                                ),
                                Text(
                                  'Register',
                                  style: TextStyle(fontSize: 15),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
  }
}
