import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/services/auth_service.dart';
import 'package:flutter_gamifit_frontend/shared/globals.dart';
import 'package:flutter_gamifit_frontend/shared/loading.dart';
import 'package:provider/provider.dart';

class Register extends StatefulWidget {
  final Function toggleView;

  Register({this.toggleView });

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {

  final _formKey = GlobalKey<FormState>();

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  // bool to change to loading screen
  bool loading = false;

  //test field state
  String email = '';
  String password = '';
  String error = '';

  //Dialog for verification mail notification
  createNotificationDialog(BuildContext context) {
    return showDialog(context: context, builder: (context) {
      return AlertDialog(
        title: Text('Please verify your Email!'),
        content: Text('We sent you a verification email! You are able to login after your email is verified!'),
        actions: [
          ElevatedButton(
              onPressed: () {
              Navigator.of(context).pop();
              widget.toggleView();
            },
            child: Text('Login')
          ),
        ],
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return loading ? Loading() : Scaffold(
      backgroundColor: Colors.black,
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Container(                                                              // White container
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(
                left: 50,
                right: 50,
                bottom: MediaQuery.of(context).size.height * 0.4 + 20,
                top: MediaQuery.of(context).size.height * 0.25),
            padding: EdgeInsets.only(left: 20, right: 20),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(20)),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    SizedBox(
                      height: 50,
                    ),
                    TextFormField(                                                // Sign up email input
                      validator: (val) => val.isEmpty ? 'Enter an email' : null,
                      onChanged: (val) {
                        setState(() {
                          email = val;
                        });
                      },
                      controller: emailController,
                      decoration: textInputDecoration.copyWith(hintText: 'Email'),
                    ),
                    TextFormField(                                                    // Sign up password input
                      validator: (val) => val.length < 6 ? 'Enter a password 6+ chars long' : null,
                      onChanged: (val) {
                        setState(() {
                          password = val;
                        });
                      },
                      obscureText: true,
                      controller: passwordController,
                      decoration: textInputDecoration.copyWith(hintText: 'Password'),
                    ),
                    SizedBox(height: 10,),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Colors.grey,
                      ),
                      child: Text('Register'),
                      onPressed: () async {
                        if (_formKey.currentState.validate()) {
                          setState(() => loading = false);
                          dynamic result = await context.read<AuthService>().register(
                              email: emailController.text.trim(),
                              password: passwordController.text.trim()
                          );
                          if (result == null) {
                            setState(() {
                              error = 'Please supply valid email!';
                              loading = false;
                            });
                          } else if (result == 'email-already-in-use') {
                            setState(() {
                              error = 'Email already in use!';
                              loading = false;
                            });
                          } else {
                            setState(() => loading = false);
                            createNotificationDialog(context);
                          }
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          Container(                                                              //Sign up Top Bar with Sign in Switch
            height: 40,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(
                left: 50,
                right: 50,
                bottom: MediaQuery.of(context).size.height * 0.4 + 20,
                top: MediaQuery.of(context).size.height * 0.25),
            padding: EdgeInsets.only(left: 20, right: 20),
            decoration: BoxDecoration(
              color: Colors.orangeAccent,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(20), topLeft: Radius.circular(20)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Register to GamiFit',
                      style: TextStyle(fontSize: 20),
                    ),
                    InkWell(
                      onTap: () {
                        widget.toggleView();
                      },
                      child: Row(
                        children: [
                          Icon(
                            Icons.person,
                            size: 15,
                          ),
                          Text(
                            'Login',
                            style: TextStyle(fontSize: 15),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
                left: 50,
                right: 50,
                bottom: MediaQuery.of(context).size.height * 0.4 + 20,
                top: MediaQuery.of(context).size.height * 0.53),
            padding: EdgeInsets.only(left: 20, right: 20),
            height: 20,
            child: Text(
              error,
              style: TextStyle(color: Colors.red, fontSize: 14.0),
            ),
          )
        ],
      ),
    );
  }
}
