import 'package:flutter/material.dart';
import 'package:flutter_gamifit_frontend/screens/authenticate/register.dart';
import 'package:flutter_gamifit_frontend/screens/authenticate/login.dart';

// Switching between login and register
class Authenticate extends StatefulWidget {
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {

  bool showSignIn = true;
  void toggleView() {                       // switch boolean for sign in or register page
    setState(() {
      showSignIn = !showSignIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (showSignIn) {
      return Login(toggleView: toggleView);
    } else {
      return Register(toggleView: toggleView);
    }
  }
}
