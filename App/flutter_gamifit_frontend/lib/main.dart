import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gamifit_frontend/wrapper.dart';
import 'package:flutter_gamifit_frontend/services/auth_service.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //init FirebaseApp
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Phone only in portrait mode
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
        providers: [
          Provider<AuthService>(
              create: (_) => AuthService(FirebaseAuth.instance)),
          // Stream provider checks on user authentication
          StreamProvider(
              create: (context) =>
                  context.read<AuthService>().authStateChanges, initialData: null,),
        ],
        child: MaterialApp(
          // debug label deactivated
          debugShowCheckedModeBanner: false,
          home: Wrapper(),
        )
    );
  }
}
